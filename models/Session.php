<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "session".
 *
 * @property int $id
 * @property string|null $session_id
 * @property string|null $data
 * @property int|null $date_touched
 */
class Session extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'session';
    }

    public function rules()
    {
        return [
            [['data'], 'string'],
            [['date_touched'], 'integer'],
            [['session_id'], 'string', 'max' => 26],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session_id' => 'Session ID',
            'data' => 'Data',
            'date_touched' => 'Date Touched',
        ];
    }
}
