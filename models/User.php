<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app-user', 'email'),
            'username' => Yii::t('app-user', 'username'),
            'name' => Yii::t('app-user', 'name'),
            'password' => Yii::t('app-user', 'password'),
            'auth_key' => Yii::t('app-user', 'auth_key'),
            'password_reset_token' => Yii::t('app-user', 'password_reset_token'),
            'access_token' => Yii::t('app-user', 'access_token'),
            'last_login' => Yii::t('app-user', 'last_login'),
            'authAssignmentNames' => Yii::t('app-user', 'authAssignmentNames'),
        ];
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findByUsername($name)
    {
        return self::findOne(['username' => $name]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::class, ["user_id" => "id"]);
    }

    public function getAuthAssignmentNames()
    {
        return implode(', ',
            array_map(function ($item) {
                return Yii::t('app-auth', $item->item_name);
            }, $this->authAssignments));
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }
}
