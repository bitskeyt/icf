<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $captcha;

    private $_user = false;

    const SCENARIO_FAILED = 'failed';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password', 'captcha'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
            [['captcha'], 'captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app-user', 'email'),
            'username' => Yii::t('app-user', 'username'),
            'name' => Yii::t('app-user', 'name'),
            'password' => Yii::t('app-user', 'password'),
            'auth_key' => Yii::t('app-user', 'auth_key'),
            'password_reset_token' => Yii::t('app-user', 'password_reset_token'),
            'access_token' => Yii::t('app-user', 'access_token'),
            'last_login' => Yii::t('app-user', 'last_login'),
            'authAssignmentNames' => Yii::t('app-user', 'authAssignmentNames'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_FAILED] = $scenarios[self::SCENARIO_DEFAULT];
        $scenarios[self::SCENARIO_DEFAULT] = array_filter($scenarios[self::SCENARIO_DEFAULT], function ($key) {
            return $key != 'captcha';
        });

        return $scenarios;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
