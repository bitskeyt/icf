<?php
namespace app\services;

use Yii;
use yii\helpers\Url;


class CommonFunc
{
	public function beforeAction(){
	    $path = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;

		if( empty( Yii::$app->user->identity->id )
				&& (array_search($path, Yii::$app->params['grantedToGuest']) === false)
			) {

			return Yii::$app->response->redirect( Url::to( ['site/login' ] ) );
		}

        return null;
	}
}
