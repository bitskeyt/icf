<?php

use yii\db\Migration;

class m200514_160719_translate extends Migration
{
    private function addWords($group, $words) {
        foreach ($words as $row) {
            $this->insert('source_message', [
                'category' => $group,
                'message' => $row[0],
            ]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('message', [
                'id' => $id,
                'language' => 'hu-HU',
                'translation' => $row[1],
            ]);
        }
    }

    public function safeUp()
    {
        $this->addWords('app', [
            ['login', 'Belépés'],
            ['logout', 'Kilépés'],
        ]);

        $this->addWords('app-menu', [
            ['admin', 'Adminisztráció'],
            ['content_editor', 'Tartalomszerkesztés'],
            ['logged_in_user', 'Felhasználói oldal'],
        ]);

        $this->addWords('app-admin', [
            ['page_title', 'Központi adminisztrációs oldal'],
        ]);

        $this->addWords('app-content-editor', [
            ['page_title', 'Tartalomszerkesztők aloldala'],
        ]);

        $this->addWords('app-logged-in-user', [
            ['page_title', 'Bejelentkezett felhasználók aloldala'],
        ]);

        $this->addWords('app-user', [
            ['page_title', 'Bejelentkezett felhasználók aloldala'],
            ['username', 'Felhasználónév'],
            ['name', 'A felhasználó neve'],
            ['password', 'Jelszó'],
            ['last_login', 'Utolsó belépés időpontja'],
            ['authAssignmentNames', 'Csoportok'],
        ]);

        $this->addWords('app-captcha', [
            ['refresh_captcha', 'Captcha frissítése'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('source_message', ['category' => 'app']);
        $this->delete('source_message', ['category' => 'app-menu']);
        $this->delete('source_message', ['category' => 'app-admin']);
        $this->delete('source_message', ['category' => 'app-content-editor']);
        $this->delete('source_message', ['category' => 'app-logged-in-user']);
        $this->delete('source_message', ['category' => 'app-user']);
        $this->delete('source_message', ['category' => 'app-captcha']);
    }
}
