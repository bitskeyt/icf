<?php

use yii\db\Migration;

class m200514_082807_create_session_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%session}}', [
            'id' => $this->char(64)->notNull(),
            'expire' => $this->integer(),
            'data' => $this->binary()
        ]);
        $this->addPrimaryKey('pk_id', '{{%session}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%session}}');
    }
}
