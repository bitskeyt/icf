<?php

use yii\db\Migration;

class m200514_133402_translate extends Migration
{
    private function addWords($group, $words) {
        foreach ($words as $row) {
            $this->insert("source_message", [
                "category" => $group,
                "message" => $row[0],
            ]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert("message", [
                "id" => $id,
                "language" => "hu-HU",
                "translation" => $row[1],
            ]);
        }
    }

    public function safeUp()
    {
        $this->addWords("app-auth", [
            ["role_admin", "Adminisztrátor"],
            ["role_content_editor", "Tartalomszerkesztő"],
            ["role_logged_in_user", "Bejelentkezett felhasználó"],
        ]);
    }

    public function safeDown()
    {
        $this->delete("source_message", ["category" => "app-auth"]);
    }
}
