<?php

use yii\db\Migration;

class m200513_121045_add_default_users extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('user', ['username', 'name', 'password'], [
            ['admin', 'Admin', Yii::$app->getSecurity()->generatePasswordHash('admin')],
            ['user1', 'User 1', Yii::$app->getSecurity()->generatePasswordHash('user1')],
            ['user2', 'User 2', Yii::$app->getSecurity()->generatePasswordHash('user2')],
            ['user3', 'User 3', Yii::$app->getSecurity()->generatePasswordHash('user3')],
        ]);
    }

    public function safeDown()
    {
        $this->truncateTable('user');
    }
}
