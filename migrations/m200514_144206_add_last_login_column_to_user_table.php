<?php

use yii\db\Migration;

class m200514_144206_add_last_login_column_to_user_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'last_login', $this->dateTime());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'last_login');
    }
}
