<?php

use yii\db\Migration;

class m200513_124523_add_authentication extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $auth = Yii::$app->authManager;

        $adminPermission = $auth->createPermission('permission_admin');
        $adminPermission->description = 'Administrator';
        $auth->add($adminPermission);

        $contentEditorPermission = $auth->createPermission('permission_content_editor');
        $contentEditorPermission->description = 'Content editor';
        $auth->add($contentEditorPermission);

        $loggedInUserPermission = $auth->createPermission('permission_logged_in_user');
        $loggedInUserPermission->description = 'Logged in user';
        $auth->add($loggedInUserPermission);

        $adminRole = $auth->createRole('role_admin');
        $adminRole->description = 'Administrator';
        $auth->add($adminRole);
        $auth->addChild($adminRole, $adminPermission);
        $auth->addChild($adminRole, $contentEditorPermission);
        $auth->addChild($adminRole, $loggedInUserPermission);

        $contentEditorRole = $auth->createRole('role_content_editor');
        $contentEditorRole->description = 'Content editor';
        $auth->add($contentEditorRole);
        $auth->addChild($contentEditorRole, $contentEditorPermission);

        $loggedInUserRole = $auth->createRole('role_logged_in_user');
        $loggedInUserRole->description = 'Logged in user';
        $auth->add($loggedInUserRole);
        $auth->addChild($loggedInUserRole, $loggedInUserPermission);

        if ($userAdmin = \app\models\User::findByUsername("admin")) {
            $auth->assign($adminRole, $userAdmin->id);
        }
        if ($user1 = \app\models\User::findByUsername("user1")) {
            $auth->assign($contentEditorRole, $user1->id);
            $auth->assign($loggedInUserRole, $user1->id);
        }
        if ($user2 = \app\models\User::findByUsername("user2")) {
            $auth->assign($contentEditorRole, $user2->id);
        }
        if ($user3 = \app\models\User::findByUsername("user3")) {
            $auth->assign($loggedInUserRole, $user3->id);
        }
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
