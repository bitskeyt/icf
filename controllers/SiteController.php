<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
//                'class' => 'app\services\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect([$this->getUsersDefaultPageOrLogin()]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if (!isset($_SESSION['failed_logins'])) {
            $_SESSION['failed_logins'] = 0;
        }

        if ($model->load(Yii::$app->request->post())) {
            // Refresh captcha
            unset($_SESSION["__captcha/site/captcha"]);
            if ($model->login()) {
                $_SESSION['failed_logins'] = 0;
                $currentUser = Yii::$app->user;
                $identity = $currentUser->getIdentity();
                $identity->last_login = $date = date("Y-m-d H:i:s");
                $identity->save();
                return $this->redirect([$this->getUsersDefaultPageOrLogin()]);
            } else {
                $_SESSION['failed_logins']++;
            }
        }

        if ($_SESSION['failed_logins'] >= 3) {
            $model->setScenario(LoginForm::SCENARIO_FAILED);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    protected function getUsersDefaultPageOrLogin()
    {
        $currentUser = Yii::$app->user;

        if ($currentUser->can('role_admin')) {
            return '/admin';
        }
        if ($currentUser->can('role_content_editor')) {
            return '/content-editor';
        }
        if ($currentUser->can('role_logged_in_user')) {
            return '/logged-in-user';
        }

        return 'login';
    }
}
