<?php

namespace app\controllers;

use app\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['permission_admin'],
                    ],
                ]
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'users' => User::find()->all()
        ]);
    }
}