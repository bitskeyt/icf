<?php

/* @var $this yii\web\View */
/* @var $userModel \yii\web\User */

use yii\widgets\DetailView;

$this->title = Yii::t('app-menu', 'admin');
?>
<div class="site-index">
    <h1 class="page-title"><?php echo Yii::t('app-admin', 'page_title') ?></h1>
    <table class="table table-bordered">
        <tr>
            <th><?php echo Yii::t('app-user', 'username') ?></th>
            <th><?php echo Yii::t('app-user', 'authAssignmentNames') ?></th>
            <th><?php echo Yii::t('app-user', 'last_login') ?></th>
        </tr>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php echo $user->username ?></td>
                <td><?php echo $user->authAssignmentNames ?></td>
                <td><?php echo $user->last_login ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
