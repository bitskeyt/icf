<?php

/* @var $this yii\web\View */
/* @var $userModel \yii\web\User */

use yii\widgets\DetailView;

$this->title = Yii::t('app-admin', 'title');
?>
<div class="site-index">
    <h1 class="page-title"><?php echo Yii::t('app-logged-in-user', 'page_title') ?></h1>
</div>
