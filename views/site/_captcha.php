<?php
use yii\helpers\Html;
use yii\captcha\Captcha;
?>

<?=
$form->field($model, 'captcha')->widget(Captcha::class, [
	'model' => $model,
	'attribute' => 'captcha',
	'imageOptions' => ['id' => 'captcha-image'],
]);
?>
<?php echo Html::button(Yii::t('app-captcha', 'refresh_captcha'), ['id' => 'refresh-captcha', 'class' => 'col-lg-offset-2']);?>
<?php $this->registerJs("
		$('#refresh-captcha').on('click', function(e){
			e.preventDefault();
	
			$('#captcha-image').yiiCaptcha('refresh');
		})
	");
?>
